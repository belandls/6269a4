import numpy as np
from matplotlib import pyplot as plt


def gaussian(x, mu, sigma):
    diff = x - mu
    num = np.exp(-0.5 * diff.T @ np.linalg.lstsq(sigma, diff)[0])
    den = np.sqrt((2*np.pi)**len(x) * np.linalg.det(sigma))
    return num/den


def ls(x, axis=None):
    off = np.max(x, axis=axis, keepdims=True)
    return off + np.log(np.sum(np.exp(x - off), axis=axis, keepdims=True))


class AbRecursionL:
    def __init__(self, k, pi, mu, sigma, A, data):
        self.k = k
        self.pi = np.log(pi)
        self.mu = mu
        self.sigma = sigma
        self.A = np.log(A)
        self.data = data
        self.alpha = np.zeros([len(data), k])
        self.beta = np.zeros([len(data), k])

    def __pxgz_v(self, x):
        res = np.zeros(self.k)
        for i in range(self.k):
            res[i] = gaussian(x, self.mu[i], self.sigma[i])
        return np.log(res)

    def __alpha(self):
        init = self.pi + self.__pxgz_v(self.data[0])
        self.alpha[0] = init
        for i in range(1, len(self.data)):
            emission = self.__pxgz_v(self.data[i])
            for j in range(self.k):
                self.alpha[i, j] = emission[j] + ls(self.A[j] + self.alpha[i-1])

    def __beta(self):
        T = len(self.data)
        init = np.ones(self.k)
        self.beta[T - 1] = init
        for i in range(T - 2, -1, -1):
            emission = self.__pxgz_v(self.data[i + 1])
            for j in range(self.k):
                self.beta[i, j] = ls(self.A[:, j] + emission + self.beta[i+1], axis=0)

    def perform_recursion(self):
        self.__alpha()
        self.__beta()

    def compute_smoothing(self, limit=None):
        return np.exp(self.alpha + self.beta - ls(self.alpha + self.beta, axis=1))

    def compute_edge_marginals(self, t):
        em = np.zeros([self.k, self.k])
        for i in range(self.k):
            for j in range(self.k):
                em[j, i] = self.alpha[t][j] + self.beta[t+1][i] + self.__pxgz_v(self.data[t+1])[i] + self.A[j, i]
        em -= ls(em, axis=(0,1))
        return np.exp(em)

    def viterbi(self):
        max = np.zeros([len(self.data), self.k])
        argmax = np.zeros([len(self.data), self.k])
        max[0] = self.pi + self.__pxgz_v(self.data[0])
        argmax[0] = np.arange(0, self.k)
        for i in range(1, len(self.data)):
            for j in range(self.k):
                max_trans = self.A[j] + max[i-1]
                max_trans += self.__pxgz_v(self.data[i])
                max[i,j] = np.max(max_trans)
                argmax[i,j] = max_trans.argsort()[-1]
        best_path = np.zeros(len(self.data), dtype=int)
        best_path[-1] = max[-1].argsort()[-1]
        for i in range(len(self.data)-2, -1, -1):
            best_path[i] = argmax[i, best_path[i+1]]
        return best_path


class HMML:

    def __init__(self, k, pi, mu, sigma, A, data):
        self.ab_rec = AbRecursionL(k, pi, mu, sigma, A, data)

    def log_likelihood(self):
        self.ab_rec.perform_recursion()
        return ls(self.ab_rec.alpha[-1])


    def single_step(self):
        #===== E-Step =========
        self.ab_rec.perform_recursion()
        current_smoothing = self.ab_rec.compute_smoothing()
        #===== M-Step =========
        # Pi
        self.ab_rec.pi = np.log(current_smoothing[0])
        #A
        weighted_A = np.zeros([self.ab_rec.k, self.ab_rec.k])
        for i in range(0,len(self.ab_rec.data)-1):
            weighted_A += self.ab_rec.compute_edge_marginals(i)
        weighted_A /= np.sum(weighted_A,axis=0, keepdims=True)
        #mu
        weighted_mu = np.zeros([self.ab_rec.data.shape[1], self.ab_rec.k])
        for i in range(len(self.ab_rec.data)):
            weighted_mu += np.outer(self.ab_rec.data[i], current_smoothing[i])
        weighted_mu /= np.sum(current_smoothing, axis=0)
        #sigma
        weighted_sigmas = [np.zeros([self.ab_rec.data.shape[1],self.ab_rec.data.shape[1]]) for _ in range(self.ab_rec.k)]
        for i in range(len(self.ab_rec.data)):
            for j in range(self.ab_rec.k):
                x = self.ab_rec.data[i]
                mu = weighted_mu[:,j]
                weighted_sigmas[j] += current_smoothing[i,j] * np.outer(x-mu, x-mu)
        s_sum = np.sum(current_smoothing, axis=0)
        for j in range(self.ab_rec.k):
            weighted_sigmas[j] /= s_sum[j]
        self.ab_rec.A = np.log(weighted_A)
        self.ab_rec.mu = [weighted_mu[:,i] for i in range(self.ab_rec.k)]
        self.ab_rec.sigma = weighted_sigmas

    def train(self, data_test, tol=1e-05):
        last_cost = 0.
        cost = self.log_likelihood()
        LL = []
        while np.abs(cost - last_cost) > tol:
            ab_temp = AbRecursionL(self.ab_rec.k, np.exp(self.ab_rec.pi),self.ab_rec.mu, self.ab_rec.sigma,np.exp(self.ab_rec.A), data_test)
            ab_temp.perform_recursion()
            test_ll = ls(ab_temp.alpha[-1])
            print('LL: {}, DIFF:{}'.format(cost, np.abs(cost - last_cost)))
            print(test_ll)
            LL.append((cost, test_ll))
            print('Normalized : {}/{}'.format(cost/len(self.ab_rec.data), test_ll/len(data_test)))
            last_cost = cost
            self.single_step()
            cost = self.log_likelihood()
        return LL

s1 = np.array([[2.9044, 0.2066], [0.2066, 2.7562]])
s2 = np.array([[0.2104, 0.2904], [0.2904, 12.2392]])
s3 = np.array([[0.9213, 0.0574], [0.0574, 1.8660]])
s4 = np.array([[6.2414, 6.0502], [6.0502, 6.1825]])
S = [s1, s2, s3, s4]

mu1 = np.array([-2.0344, 4.1726])
mu2 = np.array([3.9779, 3.7735])
mu3 = np.array([3.8007, -3.7972])
mu4 = np.array([-3.0620, -3.5345])
MU = [mu1, mu2, mu3, mu4]

k = 4
pi = np.repeat(.25, k)
diag_c = 0.5
off_diag_c = 1/6

A = np.diag(np.repeat(diag_c, k)) + (np.repeat(off_diag_c, k*k).reshape(k, k) - np.diag(np.repeat(off_diag_c, k)))

MU2 = np.vstack([mu1,mu2,mu3,mu4])




data = np.loadtxt('EMGaussian.train')
data_test = np.loadtxt('EMGaussian.test')

# ================ PART 2 =====================
ab_test_notrain = AbRecursionL(k, pi, MU, S, A, data_test)
ab_test_notrain.perform_recursion()
marginal = ab_test_notrain.compute_smoothing(100)[0:100]

x = np.arange(1, 101)
f, axes = plt.subplots(k, 1)
for i in range(k):
    axes[i].plot(x, marginal[:, i])
    axes[i].set_xlabel('t')
    axes[i].set_ylabel('p(zt={}|x_1:T)'.format(i+1))
plt.show()

# ================ /PART 2 =====================


hmm = HMML(k, pi, MU, S, A, data)
LL = hmm.train(data_test, tol=1e-05)


# ================ PART 5 =====================

x = np.arange(0, len(LL))
plt.plot(x, [ltr for ltr, lte in LL])
plt.plot(x, [lte for ltr, lte in LL])
plt.show()

# ================ /PART 5 =====================

# hmm = HMM(k, pi, MU, S, A, data)
# hmm.train(tol=1e-07)


#
# print(np.exp(hmm.ab_rec.pi))
# print(np.exp(hmm.ab_rec.A))
# print(hmm.ab_rec.mu)
# print(hmm.ab_rec.sigma)

# ================ PART 8 =====================
hmm.ab_rec.perform_recursion()
bp = hmm.ab_rec.viterbi()
# bp = hmm.ab_rec.viterbi()
clusters = [np.array([]) for _ in range(k)]
for i in range(len(bp)):
    clusters[bp[i]] = np.hstack([clusters[bp[i]], data[i]])

for i in range(k):
    clusters[i].resize([int(len(clusters[i])/2), 2])

plt.scatter(clusters[0][:, 0], clusters[0][:, 1], c='r', alpha=0.8, marker='.')
plt.scatter(clusters[1][:, 0], clusters[1][:, 1], c='b', alpha=0.8, marker='.')
plt.scatter(clusters[2][:, 0], clusters[2][:, 1], c='m', alpha=0.8, marker='.')
plt.scatter(clusters[3][:, 0], clusters[3][:, 1], c='g', alpha=0.8, marker='.')

plt.scatter(hmm.ab_rec.mu[0][0], hmm.ab_rec.mu[0][1], c='black', marker='^')
plt.scatter(hmm.ab_rec.mu[1][0], hmm.ab_rec.mu[1][1], c='black', marker='^')
plt.scatter(hmm.ab_rec.mu[2][0], hmm.ab_rec.mu[2][1], c='black', marker='^')
plt.scatter(hmm.ab_rec.mu[3][0], hmm.ab_rec.mu[3][1], c='black', marker='^')

plt.show()

# ================ /PART 8 =====================


# ================ PART 9 =====================

#k, pi, mu, sigma, A, data
ab_rec_test = AbRecursionL(hmm.ab_rec.k, np.exp(hmm.ab_rec.pi), hmm.ab_rec.mu, hmm.ab_rec.sigma, np.exp(hmm.ab_rec.A), data_test)
ab_rec_test.perform_recursion()
marginal = ab_rec_test.compute_smoothing(100)[0:100]

x = np.arange(1, 101)
f, axes = plt.subplots(k, 1)
for i in range(k):
    axes[i].plot(x, marginal[:, i])
    axes[i].set_xlabel('t')
    axes[i].set_ylabel('p(zt={}|x_1:T)'.format(i+1))
plt.show()

# ================ /PART 9 =====================

# ================ PART 10 =====================

max_marginal = marginal.argsort(axis=1)[:,-1]
plt.scatter(x, max_marginal)
plt.title('Most likely from marginal')
plt.xlabel('t')
plt.ylabel('k')
plt.show()

# ================/ PART 10 =====================

# ================ PART 11 =====================

bp_test = ab_rec_test.viterbi()[0:100]
print(np.allclose(bp_test, max_marginal))
plt.scatter(x, bp_test)
plt.title('Viterbi')
plt.xlabel('t')
plt.ylabel('k')
plt.show()

# ================ /PART 11 =====================





